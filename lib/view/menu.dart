import 'package:flutter/material.dart';
import 'package:samat_bali/view/group.dart';
import 'package:samat_bali/view/profile.dart';
import 'package:samat_bali/view/reffer.dart';

class MainMenu extends StatefulWidget {
  final VoidCallback signOut;
  MainMenu(this.signOut);

  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  signOut() {
    showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            child: Container(
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Container(
                    color: Colors.blue,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Center(
                        child: Text(
                          "Warning",
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Text(
                        "Are you sure?",
                        style: TextStyle(fontSize: 16.0),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Material(
                          color: Colors.orange,
                          borderRadius: BorderRadius.circular(10.0),
                          child: MaterialButton(
                            onPressed: () {
                              setState(() {
                                Navigator.pop(context);
                              });
                            },
                            child: Text(
                              "No",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Material(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(10.0),
                          child: MaterialButton(
                            onPressed: () {
                              Navigator.pop(context);
                              setState(() {
                                widget.signOut();
                              });
                            },
                            child: Text(
                              "Yes",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        bottomNavigationBar: TabBar(
          labelColor: Colors.blue,
          unselectedLabelColor: Colors.grey,
          tabs: <Widget>[
            Tab(
              icon: Icon(Icons.library_books),
              text: "Refferal",
            ),
            Tab(
              icon: Icon(Icons.people),
              text: "Group",
            ),
            Tab(
              icon: Icon(Icons.person),
              text: "Profile",
            )
          ],
        ),
        appBar: AppBar(
          leading: Icon(
            Icons.person,
            color: Colors.white,
          ),
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: Center(
            child: Image.asset(
              "./assets/img/logo_title.png",
              height: 40.0,
            ),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.lock_open,
                color: Colors.grey,
              ),
              onPressed: () {
                setState(() {
                  signOut();
                });
              },
            )
          ],
        ),
        body: TabBarView(
          children: <Widget>[Reffer(), GroupPage(), ProfilePage()],
        ),
      ),
    );
  }
}
