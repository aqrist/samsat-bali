import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:samat_bali/model/STNKModel.dart';
import 'package:samat_bali/provider/config.dart';
import 'package:samat_bali/view/stnk_update.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class ViewBPKB extends StatefulWidget {
  @override
  _ViewBPKBState createState() => _ViewBPKBState();
}

class _ViewBPKBState extends State<ViewBPKB> {
  var loading = false;
  String idUsers;

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      idUsers = preferences.getString("id");
      print(idUsers);
    });
    _fetchData();
  }

  List<STNKModel> _list = [];

  Future<Null> _fetchData() async {
    setState(() {
      loading = true;
    });
    final response = await http.get(BaseUrl.bpkbview + idUsers);
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      setState(() {
        for (Map i in data) {
          _list.add(STNKModel.fromJson(i));
        }
        loading = false;
      });
    }
    print(BaseUrl.bpkbview + idUsers);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Data STNK"),
      ),
      body: Container(
        child: loading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: _list.length,
                itemBuilder: (context, i) {
                  final a = _list[i];
                  int _idjenis = int.parse(a.jenis);
                  String jenis;
                  var oCcy = new NumberFormat("###.0#", "en_US");
                  if (_idjenis == 1) {
                    jenis = 'Motor';
                  } if(_idjenis == 2) {
                    jenis = 'Mobil';
                  }

                  return Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 16.0),
                        child: Text(
                          "Info Kendaraan",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                      ListTile(
                        title: Text("Jenis Kendaraan"),
                        trailing: Text(jenis ?? ''),
                      ),
                      ListTile(
                        title: Text("Nomor Polisi"),
                        trailing: Text(a.noPolisi ?? ''),
                      ),
                      ListTile(
                        title: Text("Nama Pemilik"),
                        trailing: Text(a.namaPemilik ?? ''),
                      ),
                      ListTile(
                        title: Text("Alamat"),
                        trailing: Text(a.alamat ?? ''),
                      ),
                      ListTile(
                        title: Text("Nomor BPKB"),
                        trailing: Text(a.noBPKB ?? ''),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                        child: SizedBox(
                          height: 1.0,
                          child: Container(
                            color: Colors.black,
                          ),
                        ),
                      ),
                      Text(
                        "Info Pajak",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18.0,
                        ),
                      ),
                      ListTile(
                        title: Text("PKB"),
                        trailing: Text(a.pkb ?? ''),
                      ),
                      ListTile(
                        title: Text("SWDKLLJ"),
                        trailing: Text(a.swdkllj ?? ''),
                      ),
                      ListTile(
                        title: Text("Admin STNK"),
                        trailing: Text(a.admstnk ?? ''),
                      ),
                      ListTile(
                        title: Text("Admin TNKB"),
                        trailing: Text(a.admtnkb ?? ''),
                      ),
                      ListTile(
                          title: Text("Jumlah"),
                          trailing: Text(
                            "jumlah",
                            // "Rp. ${_jumlah}",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )),
                      Material(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.blue,
                        child: MaterialButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => STNKUpdate()));
                          },
                          child: Text(
                            "Update Data STNK",
                            style:
                                TextStyle(color: Colors.white, fontSize: 18.0),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      )
                    ],
                  );
                },
              ),
      ),
    );
  }
}
