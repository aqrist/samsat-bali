import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:samat_bali/provider/config.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class STNKAdd extends StatefulWidget {
  @override
  _STNKAddState createState() => _STNKAddState();
}

class _STNKAddState extends State<STNKAdd> {
  String idUsers,
      jenis,
      noPolisi,
      namaPemilik,
      alamat,
      noBPKB,
      pkb,
      swdkllj,
      admstnk,
      admtnkb;
  final _key = new GlobalKey<FormState>();

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      idUsers = preferences.getString("id");
      print(idUsers);
    });
  }

  @override
  initState() {
    super.initState();
    getPref();
  }

  check() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      submit();
    }
  }

  var loading = false;
  submit() async {
    setState(() {
      loading = true;
    });
    final response = await http.post(BaseUrl.bpkb_add, body: {
      "idUsers": idUsers,
      "jenis": jenis,
      "noPolisi": noPolisi.trim(),
      "namaPemilik": namaPemilik,
      "alamat": alamat,
      "noBPKB": noBPKB.trim(),
      "pkb": pkb,
      "swdkllj": swdkllj,
      "admstnk": admstnk,
      "admtnkb": admtnkb
    });
    final data = jsonDecode(response.body);
    int value = data['value'];
    String pesan = data['message'];
    if (value == 3) {
      print(response.body);
      setState(() {
        loading = false;
        Navigator.pop(context);
      });
    } else {
      print(response.body);
      showLongToast(pesan);
    }
  }

  showLongToast(String pesan) {
    Fluttertoast.showToast(msg: "$pesan", toastLength: Toast.LENGTH_LONG);
  }

  int _radioValue = 0;
  int _result = 0;

  _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
      switch (_radioValue) {
        case 1:
          _result = 1;
          setState(() {
            jenis = '1';
          });
          break;
        case 2:
          _result = 2;
          setState(() {
            jenis = '2';
          });
          break;
      }
      print(_radioValue);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Update Data STNK"),
      ),
      body: Center(
        child: Form(
          key: _key,
          child: ListView(
            // physics: NeverScrollableScrollPhysics(),
            padding: EdgeInsets.all(16.0),
            // shrinkWrap: true,
            children: <Widget>[
              Text(
                "Info Kendaraan",
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Radio(
                    value: 1,
                    groupValue: _radioValue,
                    onChanged: _handleRadioValueChange,
                  ),
                  Icon(
                    Icons.directions_car,
                    size: 36.0,
                  ),
                  SizedBox(
                    width: 24.0,
                  ),
                  Radio(
                    value: 2,
                    groupValue: _radioValue,
                    onChanged: _handleRadioValueChange,
                  ),
                  Icon(
                    Icons.directions_bike,
                    size: 36.0,
                  ),
                ],
              ),
              TextFormField(
                onSaved: (e) => noPolisi = e,
                decoration: InputDecoration(
                  labelText: "Nomor Polisi",
                ),
                // keyboardType: TextInputType.text,
              ),
              TextFormField(
                onSaved: (e) => namaPemilik = e,
                decoration: InputDecoration(
                  labelText: "Nama Pemilik",
                ),
                // keyboardType: TextInputType.text,
              ),
              TextFormField(
                onSaved: (e) => alamat = e,
                decoration: InputDecoration(
                  labelText: "Alamat",
                ),
                // keyboardType: TextInputType.text,
              ),
              TextFormField(
                onSaved: (e) => noBPKB = e,
                decoration: InputDecoration(
                  labelText: "Nomor BPKB",
                ),
                // keyboardType: TextInputType.text,
              ),
              SizedBox(
                height: 24.0,
              ),
              Text(
                "Info Pajak",
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
              TextFormField(
                onSaved: (e) => pkb = e,
                decoration: InputDecoration(
                  labelText: "PKB",
                ),
                // keyboardType: TextInputType.number,
              ),
              TextFormField(
                onSaved: (e) => swdkllj = e,
                decoration: InputDecoration(
                  labelText: "SWDKLLJ",
                ),
                // keyboardType: TextInputType.number,
              ),
              TextFormField(
                onSaved: (e) => admstnk = e,
                decoration: InputDecoration(
                  labelText: "Admin STNK",
                ),
                // keyboardType: TextInputType.number,
              ),
              TextFormField(
                onSaved: (e) => admtnkb = e,
                decoration: InputDecoration(
                  labelText: "Admin TNKB",
                ),
                // keyboardType: TextInputType.number,
              ),
              SizedBox(
                height: 24.0,
              ),
              Material(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.blue,
                child: MaterialButton(
                  onPressed: () {
                    check();
                    print(jenis);
                  },
                  child: Text(
                    "Update",
                    style: TextStyle(color: Colors.white, fontSize: 18.0),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
