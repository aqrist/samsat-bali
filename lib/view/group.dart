import 'package:flutter/material.dart';
import 'package:samat_bali/model/groupsModel.dart';
import 'package:samat_bali/provider/config.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class GroupPage extends StatefulWidget {
  @override
  _GroupPageState createState() => _GroupPageState();
}

class _GroupPageState extends State<GroupPage> {
  var loading = false;
  String idUsers, level, idSTNK;

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      idUsers = preferences.getString("id");
      level =preferences.getString("level");
      idSTNK =preferences.getString("idSTNK");
      // print(idUsers);
      // print(level);
      print(idSTNK);
    });
    _fetchData();
  }

  List<GroupsModel> _list = [];

  Future<Null> _fetchData() async {
    setState(() {
      loading = true;
    });
    final response = await http.get(BaseUrl.groups + idUsers);
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      setState(() {
        for (Map i in data) {
          _list.add(GroupsModel.fromJson(i));
        }
        loading = false;
      });
    }
    print(BaseUrl.groups+idUsers);
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: loading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              itemCount: _list.length,
              itemBuilder: (context, i) {
                final a = _list[i];
                return Card(
                  elevation: 1.0,
                  child: ListTile(
                    leading: Icon(
                      Icons.account_circle,
                      color: Colors.blue,
                      size: 48.0,
                    ),
                    title: Text("Member : " + a.namaMember),
                    subtitle: Text("Admin : " + a.namaAdmin),
                  ),
                );
              },
            ),
    );
  }
}

