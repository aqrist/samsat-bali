import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:samat_bali/provider/config.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:samat_bali/view/stnk_add.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  String email, password, nama, phone;
  bool _obscureText = true;
  final _key = new GlobalKey<FormState>();

  check() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      submit();
    }
  }

  var loading = false;
  submit() async {
    setState(() {
      loading = true;
    });
    final response = await http.post(BaseUrl.daftar, body: {
      "username": email.trim(),
      "password": password.trim(),
      "nama": nama.trim(),
      "phone": phone.trim()
    });
    final data = jsonDecode(response.body);
    int value = data['value'];
    String pesan = data['message'];
    if (value == 6) {
      print(response.body);
      setState(() {
        loading = false;
        Navigator.pop(context);
        // Navigator.pushReplacement(context, 
        // MaterialPageRoute(builder: (context) => STNKAdd()));
      });
    } else {
      print(response.body);
      showLongToast(pesan);
    }
  }

  showLongToast(String pesan) {
    Fluttertoast.showToast(
      msg: "$pesan", 
      toastLength: Toast.LENGTH_LONG);
  }

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: double.infinity,
          ),
          Container(
            width: double.infinity,
            height: 148.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("./assets/img/wallpaper.jpg"),
                  fit: BoxFit.cover),
            ),
            child: Container(
              padding: EdgeInsets.only(left: 8.0, top: 64.0),
              decoration: BoxDecoration(color: Colors.blue.withOpacity(0.3)),
              child: ListTile(
                leading: Image.asset(
                  "./assets/img/logo_eso.png",
                  height: 80.0,
                ),
                title: Text(
                  "Register",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                      color: Colors.white),
                ),
                subtitle: Text(
                  "dapatkan referral code setelah mendaftar",
                  style: TextStyle(fontSize: 14.0, color: Colors.white),
                ),
              ),
            ),
          ),
          Positioned(
            top: 148.0,
            left: 0.0,
            right: 0.0,
            child: Form(
              key: _key,
              child: ListView(
                physics: NeverScrollableScrollPhysics(),
                padding: EdgeInsets.all(16.0),
                shrinkWrap: true,
                children: <Widget>[
                  TextFormField(
                    onSaved: (e) => email = e,
                    decoration: InputDecoration(
                      labelText: "Email Address",
                      icon: Icon(Icons.email),
                    ),
                    keyboardType: TextInputType.emailAddress,
                  ),
                  TextFormField(
                    onSaved: (e) => nama = e,
                    decoration: InputDecoration(
                        labelText: "Fullname",
                        icon: Icon(Icons.account_circle)),
                    keyboardType: TextInputType.text,
                  ),
                  TextFormField(
                    onSaved: (e) => phone = e,
                    decoration: InputDecoration(
                        labelText: "Phone Number", icon: Icon(Icons.call)),
                    keyboardType: TextInputType.phone,
                  ),
                  TextFormField(
                    onSaved: (e) => password = e,
                    obscureText: _obscureText,
                    decoration: InputDecoration(
                        labelText: "Password",
                        icon: Icon(Icons.lock),
                        suffixIcon: IconButton(
                          onPressed: _toggle,
                          icon: Icon(_obscureText
                              ? Icons.visibility
                              : Icons.visibility_off),
                        )),
                    keyboardType: TextInputType.text,
                  ),
                  SizedBox(
                    height: 24.0,
                  ),
                  Material(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.blue,
                    child: MaterialButton(
                      onPressed: () {
                        check();
                        // Navigator.pop(context);
                      },
                      child: Text(
                        "Sign up",
                        style: TextStyle(color: Colors.white, fontSize: 18.0),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
