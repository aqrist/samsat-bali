import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:samat_bali/provider/config.dart';
import 'package:samat_bali/view/menu.dart';
import 'package:samat_bali/view/register.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

enum AuthStatus { signIn, notSignIn }

class _LoginPageState extends State<LoginPage> {
  String username, password;
  final _key = new GlobalKey<FormState>();

  AuthStatus auth = AuthStatus.notSignIn;

  check() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      submit();
    }
  }

  var loading = false;

  submit() async {
    setState(() {
      loading = true;
    });
    final response = await http.post(BaseUrl.login, body: {
      "username" : username,
      "password" : password
    });
    final data = jsonDecode(response.body);
    int value = data['value'];
    String pesan = data['message'];
    String id = data['id'];
    String usernameBaru = data['username'];
    String level = data['level'];
    String status = data['status'];
    String kode = data['kode'];
    String nama = data['nama'];
    String photo = data['photo'];
    String phone = data['phone'];
    String jk = data['jk'];
    String saldo = data['saldo'];
    String idSTNK = data['idSTNK'];
    if (value==1) {
      setState(() {
        loading = false;
        savePref(id, usernameBaru, level, status, kode, nama, photo, phone, jk, saldo, idSTNK);
        auth = AuthStatus.signIn;
      });
    } else {
      print(response.body);
      showLongToast(pesan);
      setState(() {
        loading = false;
      });
    }
  }

  showLongToast(String pesan) {
    Fluttertoast.showToast(
      msg: "$pesan",
      toastLength: Toast.LENGTH_LONG,
    );
  }

  savePref(String id, String username, String level, String status, String kode,
      String nama, String photo, String phone, String jk, String saldo, String idSTNK) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setString("id", id);
      preferences.setString("username", username);
      preferences.setString("level", level);
      preferences.setString("status", status);
      preferences.setString("kode", kode);
      preferences.setString("nama", nama);
      preferences.setString("photo", photo);
      preferences.setString("phone", phone);
      preferences.setString("jk", jk);
      preferences.setString("saldo", saldo);
      preferences.setString("idSTNK", idSTNK);
      // preferences.commit();
    });
  }

  bool _obscureText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  var level;

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      level = preferences.getString("level");
      auth = level == "1" ? AuthStatus.signIn : AuthStatus.notSignIn;
    });
    print(level);
  }

  signOut() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setString("id", null);
      preferences.setString("username", null);
      preferences.setString("level", null);
      preferences.setString("status", null);
      preferences.setString("kode", null);
      preferences.setString("nama", null);
      preferences.setString("photo", null);
      preferences.setString("phone", null);
      preferences.setString("jk", null);
      preferences.setString("saldo", null);
      preferences.setString("idSTNK", null);
      auth = AuthStatus.notSignIn;
    });
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    switch (auth) {
      case AuthStatus.notSignIn:
        return Scaffold(
          body: Stack(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: double.infinity,
              ),
              Container(
                width: double.infinity,
                height: 180.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("./assets/img/wallpaper.jpg"),
                      fit: BoxFit.cover),
                ),
                child: new Container(
                  decoration:
                      BoxDecoration(color: Colors.blue.withOpacity(0.3)),
                ),
              ),
              Positioned(
                top: 120.0,
                left: 0.0,
                right: 0.0,
                child: Form(
                  key: _key,
                  child: ListView(
                    padding: EdgeInsets.all(16.0),
                    shrinkWrap: true,
                    children: <Widget>[
                      Image.asset(
                        "./assets/img/logo_eso.png",
                        height: 90.0,
                      ),
                      TextFormField(
                        onSaved: (e) => username = e,
                        decoration: InputDecoration(
                            labelText: "Username / Phone Number",
                            icon: Icon(Icons.account_circle)),
                      ),
                      TextFormField(
                        obscureText: _obscureText,
                        onSaved: (e) => password = e,
                        decoration: InputDecoration(
                            labelText: "Password",
                            icon: Icon(Icons.lock),
                            suffixIcon: IconButton(
                              onPressed: _toggle,
                              icon: Icon(_obscureText
                                  ? Icons.visibility
                                  : Icons.visibility_off),
                            )),
                      ),
                      SizedBox(
                        height: 54.0,
                      ),
                      Material(
                        elevation: 2.0,
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.blue,
                        child: MaterialButton(
                          onPressed: () {
                            // login
                            check();
                          },
                          child: Text(
                            "Log In",
                            style:
                                TextStyle(color: Colors.white, fontSize: 18.0),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 16.0,
                      ),
                      Material(
                        elevation: 2.0,
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.white,
                        child: MaterialButton(
                          onPressed: () {},
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image.asset(
                                "./assets/img/google.png",
                                height: 23.0,
                              ),
                              Text(
                                " Log In with Google",
                                style: TextStyle(
                                    color: Colors.grey, fontSize: 18.0),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 16.0,
                      ),
                      Material(
                        elevation: 2.0,
                        borderRadius: BorderRadius.circular(10.0),
                        color: Color(0xFF3b5998),
                        child: MaterialButton(
                          onPressed: () {},
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image.asset(
                                "./assets/img/fb_white.png",
                                height: 23.0,
                              ),
                              Text(
                                "  Log In with Facebook",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18.0),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Center(
                        child: Text("Or"),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Register()));
                              },
                              child: Text(
                                "Create an Account",
                                style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18.0),
                                textAlign: TextAlign.center,
                              )),
                          Text("|"),
                          InkWell(
                              onTap: () {},
                              child: Text(
                                " Forgot Password ? ",
                                style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18.0),
                                textAlign: TextAlign.center,
                              )),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              Positioned(
                top: 0.0,
                left: 0.0,
                right: 0.0,
                bottom: 0.0,
                child: loading
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : SizedBox(),
              )
            ],
          ),
        );

        break;
      case AuthStatus.signIn:
        return MainMenu(signOut);
        break;
    }
  }
}
