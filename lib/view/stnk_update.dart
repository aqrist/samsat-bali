import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:samat_bali/provider/config.dart';
import 'package:samat_bali/view/login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class STNKUpdate extends StatefulWidget {
  @override
  _STNKUpdateState createState() => _STNKUpdateState();
}

class _STNKUpdateState extends State<STNKUpdate> {
  String idSTNK;
  String idUsers,
      jenis,
      noPolisi,
      namaPemilik,
      alamat,
      noBPKB,
      pkb,
      swdkllj,
      admstnk,
      admtnkb;
  final _key = new GlobalKey<FormState>();

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      idUsers = preferences.getString("id");
      idSTNK = preferences.getString("idSTNK");
      print(idSTNK);
    });
  }

  check() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      submit();
    }
  }

  var loading = false;
  submit() async {
    setState(() {
      loading = true;
    });
    final response = await http.post(BaseUrl.bpkb_update, body: {
      "idSTNK": idSTNK,
      "jenis": jenis,
      "noPolisi": noPolisi.trim(),
      "namaPemilik": namaPemilik,
      "alamat": alamat,
      "noBPKB": noBPKB.trim(),
      "pkb": pkb.trim(),
      "swdkllj": swdkllj.trim(),
      "admstnk": admstnk.trim(),
      "admtnkb": admtnkb.trim(),
    });
    final data = jsonDecode(response.body);
    int value = data['value'];
    String pesan = data['message'];
    if (value == 3) {
      print(response.body);
      setState(() {
        loading = false;
        Navigator.pop(context);
        // Navigator.pushReplacement(
        //     context, MaterialPageRoute(builder: (context) => LoginPage()));
      });
    } else {
      print(response.body);
      showLongToast(pesan);
    }
  }

  showLongToast(String pesan) {
    Fluttertoast.showToast(msg: "$pesan", toastLength: Toast.LENGTH_LONG);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPref();
    print(idSTNK);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit STNK"),
      ),
      body: Center(
        child: Form(
          key: _key,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  onSaved: (e) => jenis = e,
                  decoration: InputDecoration(hintText: "Jenis Kendaraan"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  onSaved: (e) => noPolisi = e,
                  decoration: InputDecoration(hintText: "Nomor Polisi"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  onSaved: (e) => namaPemilik = e,
                  decoration: InputDecoration(hintText: "Nama Pemilik"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  onSaved: (e) => alamat = e,
                  decoration: InputDecoration(hintText: "Alamat"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  onSaved: (e) => noBPKB = e,
                  decoration: InputDecoration(hintText: "Nomor BPKB"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  onSaved: (e) => pkb = e,
                  decoration: InputDecoration(hintText: "PKB"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  onSaved: (e) => swdkllj = e,
                  decoration: InputDecoration(hintText: "SWDKLLJ"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  onSaved: (e) => admstnk = e,
                  decoration: InputDecoration(hintText: "Admin STNK"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  onSaved: (e) => admtnkb = e,
                  decoration: InputDecoration(hintText: "Admin TNKB"),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Material(
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.blue,
                child: MaterialButton(
                  onPressed: () {
                    check();
                    // Navigator.pop(context);
                  },
                  child: Text(
                    "Update",
                    style: TextStyle(color: Colors.white, fontSize: 18.0),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
