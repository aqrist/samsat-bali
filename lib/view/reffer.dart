import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:share/share.dart';

class Reffer extends StatefulWidget {
  @override
  _RefferState createState() => _RefferState();
}

class _RefferState extends State<Reffer> {
  var loading = false;
  String kode;

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      kode = preferences.getString("kode");
      print(kode);
    });
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            width: double.maxFinite,
            height: double.maxFinite,
            decoration: BoxDecoration(
              color: Colors.blue,
              image: DecorationImage(
                colorFilter: ColorFilter.mode(
                    Colors.blue.withOpacity(0.2), BlendMode.dstATop),
                image: AssetImage("./assets/img/green.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: Container(
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 24.0),
              child: FittedBox(
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      "./assets/img/rewards.png",
                      height: 172.0,
                    ),
                    SizedBox(
                      height: 48.0,
                    ),
                    Text(
                      "Reffer this code to your friends, earn bla bla",
                      style: TextStyle(color: Colors.white, fontSize: 16.0),
                    ),
                    SizedBox(
                      height: 24.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Container(
                        child: GestureDetector(
                          onTap: () {
                            Share.share(
                                'http://samsat.whitaaplikasi.com/invite/referal.php?kr=' +
                                    kode);
                          },
                          child: Container(
                            height: 50.0,
                            width: 300.0,
                            decoration: BoxDecoration(
                                color: Colors.green,
                                border:
                                    Border.all(color: Colors.white, width: 2.0),
                                borderRadius: BorderRadius.circular(10.0)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "Share to friends",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20.0),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 16.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Container(
                        child: GestureDetector(
                          onTap: _refferUrl,
                          child: Container(
                            height: 50.0,
                            width: 300.0,
                            decoration: BoxDecoration(
                                color: Colors.orange,
                                border:
                                    Border.all(color: Colors.white, width: 2.0),
                                borderRadius: BorderRadius.circular(10.0)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "Tap this Link",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20.0),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 48.0, right: 48.0, top: 24.0),
                      child: Text(
                        "Invite your friends to join ESO and get bla bla bla, \n cashback bla bla bla, for each friend \n that join using your referral code",
                        style: TextStyle(color: Colors.white, fontSize: 16.0),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  _refferUrl() async {
    final url = 'http://samsat.whitaaplikasi.com/invite/referal.php?kr=' + kode;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Mohon maaf akses mengalami kendala $url';
    }
  }
}
