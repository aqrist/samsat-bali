import 'package:flutter/material.dart';
import 'package:get_version/get_version.dart';
import 'package:flutter/services.dart';
import 'package:samat_bali/view/stnk_add.dart';
import 'package:samat_bali/view/stnk_simply.dart';
import 'package:samat_bali/view/stnk_view.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  var loading = false;
  String _projectVersion = '';
  String _projectCode = '';
  String nama, idSTNK;

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      loading = true;
      nama = preferences.getString("nama");
      idSTNK = preferences.getString("idSTNK");
      print(idSTNK);
    });
  }
  

  @override
  initState() {
    super.initState();
    initPlatformState();
    getPref();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await GetVersion.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    String projectVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      projectVersion = await GetVersion.projectVersion;
    } on PlatformException {
      projectVersion = 'Failed to get project version.';
    }

    String projectCode;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      projectCode = await GetVersion.projectCode;
    } on PlatformException {
      projectCode = 'Failed to get build number.';
    }

    String projectAppID;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      projectAppID = await GetVersion.appID;
    } on PlatformException {
      projectAppID = 'Failed to get app ID.';
    }

    String projectName;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      projectName = await GetVersion.appName;
    } on PlatformException {
      projectName = 'Failed to get app name.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      // _platformVersion = platformVersion;
      _projectVersion = projectVersion;
      // _projectCode = projectCode;
      // _projectAppID = projectAppID;
      // _projectName = projectName;
    });
  }

  @override
  Widget build(BuildContext context) {
    

    _addSTNK() {
      if (idSTNK == null) {
        return Padding(
        padding: const EdgeInsets.only(right: 20.0, left: 20.0),
        child: Card(
          elevation: 2.0,
          child: ListTile(
            title: Text(
              "Add Data STNK",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => STNKAdd()));
              // builder: (context) => FormBPKBSimply()));
            },
          ),
        ),
      );
      } else {
        return SizedBox();
      }
    }

    loading = false;
    return Container(
      child: loading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView(
              children: <Widget>[
                Card(
                  elevation: 0.0,
                  child: ListTile(
                    leading: Icon(
                      Icons.account_circle,
                      size: 54.0,
                      color: Colors.blue,
                    ),
                    title: Text(
                      nama ?? "",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text("Beginner - 900 Points"),
                  ),
                ),
                SizedBox(
                  height: 24.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                  child: Card(
                    elevation: 2.0,
                    child: ListTile(
                      title: Text("Edit Profile"),
                      trailing: Icon(Icons.arrow_forward_ios),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                  child: Card(
                    elevation: 2.0,
                    child: ListTile(
                      title: Text("Change Password"),
                      trailing: Icon(Icons.arrow_forward_ios),
                    ),
                  ),
                ),
                // ini harusnya ilang kalo data idSTNK engga null
                _addSTNK(),
                // hayolo
                Padding(
                  padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                  child: Card(
                    elevation: 2.0,
                    child: ListTile(
                      title: Text(
                        "Data STNK",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      trailing: Icon(Icons.arrow_forward_ios),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ViewBPKB()));
                        // builder: (context) => FormBPKBSimply()));
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                  child: Card(
                    elevation: 2.0,
                    child: ListTile(
                      title: Text("History"),
                      trailing: Icon(Icons.arrow_forward_ios),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                  child: Card(
                    elevation: 2.0,
                    child: ListTile(
                      title: Text("App Version"),
                      subtitle: Text(_projectVersion),
                      // trailing: Icon(Icons.arrow_forward_ios),
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}
