class GroupsModel {
  final String id;
  final String idUsers;
  final String id_member;
  final String namaAdmin;
  final String namaMember;
  final String status;

  GroupsModel(
    {this.id,
    this.idUsers,
    this.id_member,
    this.namaAdmin,
    this.namaMember,
    this.status}
  );

  factory GroupsModel.fromJson(Map<String, dynamic> json) {
    return GroupsModel(
      id: json['id'],
      idUsers: json['idUsers'],
      id_member: json['id_member'],
      namaAdmin: json['namaAdmin'],
      namaMember: json['namaMember'],
      status: json['status'],
    );
  }
}
