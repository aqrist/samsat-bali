class STNKModel {
  final String id;
  final String username;
  final String level;
  final String status;
  final String kode;
  final String nama;
  final String photo;
  final String phone;
  final String jk;
  final String saldo;
  final String jenis;
  final String noPolisi;
  final String namaPemilik;
  final String alamat;
  final String thPembuatan;
  final String warnaKB;
  final String noRangka;
  final String noMesin;
  final String coding;
  final String noBPKB;
  final String bahanBakar;
  final String warnaTNKB;
  final String fotoSTNK;
  final String fotoKendaraan;
  final String merk;
  final String model;
  final String isiSilinder;
  final String pkb;
  final String swdkllj;
  final String admstnk;
  final String admtnkb;
  // final String jumlah;

  STNKModel({
    this.id,
    this.username,
    this.level,
    this.status,
    this.kode,
    this.nama,
    this.photo,
    this.phone,
    this.jk,
    this.saldo,
    this.jenis,
    this.noPolisi,
    this.namaPemilik,
    this.alamat,
    this.thPembuatan,
    this.warnaKB,
    this.noRangka,
    this.noMesin,
    this.coding,
    this.noBPKB,
    this.bahanBakar,
    this.warnaTNKB,
    this.fotoSTNK,
    this.fotoKendaraan,
    this.merk,
    this.model,
    this.isiSilinder,
    this.pkb,
    this.swdkllj,
    this.admstnk,
    this.admtnkb,
    // this.jumlah
  });

  factory STNKModel.fromJson(Map<String, dynamic> json) {
    return STNKModel(
      id:json['id'],
      username: json['username'],
      level: json['level'],
      status: json['status'],
      kode: json['kode'],
      nama: json['nama'],
      photo: json['photo'],
      phone: json['phone'],
      jk: json['jk'],
      saldo: json['saldo'],
      jenis:json['jenis'],
      noPolisi:json['noPolisi'],
      namaPemilik:json['namaPemilik'],
      alamat:json['alamat'],
      thPembuatan:json['thPembuatan'],
      warnaKB:json['warnaKB'],
      noRangka:json['noRangka'],
      noMesin:json['noMesin'],
      coding:json['coding'],
      noBPKB:json['noBPKB'],
      bahanBakar:json['bahanBakar'],
      warnaTNKB:json['warnaTNKB'],
      fotoSTNK: json['fotoSTNK'],
      fotoKendaraan: json['fotoKendaraan'],
      merk:json['merk'],
      model:json['model'],
      isiSilinder:json['isiSilinder'],
      pkb:json['pkb'],
      swdkllj:json['swdkllj'],
      admstnk:json['admstnk'],
      admtnkb:json['admtnkb'],
      // jumlah:json['jumlah'],
    );
  }
}
