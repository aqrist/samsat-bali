class KendaraanModel {
  final String id;
  final String idUsers;
  final String jenis;
  final String noPolisi;
  final String namaPemilik;
  final String alamat;
  final String merk;
  final String model;
  final String thPembuatan;
  final String warnaKB;
  final String isiSilinder;
  final String noRangka;
  final String noMesin;
  final String coding;
  final String noBPKB;
  final String bahanBakar;
  final String warnaTNKB;
  final String harga;
  final String pkb;
  final String idKendaraan;
  final String swdkllj;
  final String admstnk;
  final String admtnkb;

  KendaraanModel({
    this.id,
    this.idUsers,
    this.jenis,
    this.noPolisi,
    this.namaPemilik,
    this.alamat,
    this.merk,
    this.model,
    this.thPembuatan,
    this.warnaKB,
    this.isiSilinder,
    this.noRangka,
    this.noMesin,
    this.coding,
    this.noBPKB,
    this.bahanBakar,
    this.warnaTNKB,
    this.harga,
    this.pkb,
    this.idKendaraan,
    this.swdkllj,
    this.admstnk,
    this.admtnkb,}
  );

  factory KendaraanModel.fromJson(Map<String, dynamic> json) {
    return KendaraanModel(
      id:json['id'],
      idUsers:json['idUsers'],
      jenis:json['jenis'],
      noPolisi:json['noPolisi'],
      namaPemilik:json['namaPemilik'],
      alamat:json['alamat'],
      merk:json['merk'],
      model:json['model'],
      thPembuatan:json['thPembuatan'],
      warnaKB:json['warnaKB'],
      isiSilinder:json['isiSilinder'],
      noRangka:json['noRangka'],
      noMesin:json['noMesin'],
      coding:json['coding'],
      noBPKB:json['noBPKB'],
      bahanBakar:json['bahanBakar'],
      warnaTNKB:json['warnaTNKB'],
      harga:json['harga'],
      pkb:json['pkb'],
      idKendaraan:json['idKendaraan'],
      swdkllj:json['swdkllj'],
      admstnk:json['admstnk'],
      admtnkb:json['admtnkb'],
    );
  }
}
