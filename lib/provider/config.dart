class BaseUrl {
  // live
  // static String login =
  //     "http://samsat.whitaaplikasi.com/webservices/login.php";
  // static String daftar =
  //     "http://samsat.whitaaplikasi.com/webservices/daftar.php";
  // static String groups =
  //     "http://samsat.whitaaplikasi.com/webservices/groups.php?idUsers=";
  
  // local 
  static String login =
      "http://192.168.64.2/samsatbali/webservices/login.php";
  static String daftar =
      "http://192.168.64.2/samsatbali/webservices/daftar.php";
  static String groups =
      "http://192.168.64.2/samsatbali/webservices/groups.php?idUsers=";
  static String bpkb_add =
      "http://192.168.64.2/samsatbali/webservices/bpkb.php";
  static String bpkbview =
      "http://192.168.64.2/samsatbali/webservices/bpkb_view.php?idUsers=";
  static String bpkb_update =
      "http://192.168.64.2/samsatbali/webservices/bpkb_update.php";
  
  // static String lupaPassword =
  //     "http://esopos.whitaaplikasi.com/live/webservices/lupaPassword.php";
  // static String confirm =
  //     "http://esopos.whitaaplikasi.com/live/webservices/confirmationeso.php";
  // static String resetPassword =
  //     "http://esopos.whitaaplikasi.com/live/webservices/updatenewpassword.php";
  // static String cekSaldo =
  //     "http://esopos.whitaaplikasi.com/live/webservices/ceksaldoeso.php";
  // static String getTransaksi =
  //     "http://esopos.whitaaplikasi.com/live/webservices/getTransaksiUsersEso.php?apiToken=";

  // static String getDetailitemTransaksi =
  //     "http://esopos.whitaaplikasi.com/live/webservices/getdetailitemesousers.php?apiToken=";
  // static String pathImgProfile = "http://esopos.whitaaplikasi.com/live/img/";
  // static String pathImgProduk = "http://esopos.whitaaplikasi.com/produk/";
}
