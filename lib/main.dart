import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:samat_bali/view/login.dart';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: LoginPage(),
    theme: ThemeData(primarySwatch: Colors.blue),
  ));
}